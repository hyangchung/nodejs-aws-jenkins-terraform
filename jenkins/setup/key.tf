resource "aws_key_pair" "hyachung-keypair" {
  key_name   = "hyachung-keypair"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
  lifecycle {
    ignore_changes = [public_key]
  }
}

