terraform {
  backend "s3" {
    bucket = "hyachung-node-aws-jenkins-terraform"
    key    = "jenkins.terraform.tfstate"
    region = "eu-west-1"
  }
}

